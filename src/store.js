import { configureStore } from '@reduxjs/toolkit';
import doctorReducer from "./redux/doctorReducer";
export default configureStore({
    reducer: {
        doctors: doctorReducer
    },
});
