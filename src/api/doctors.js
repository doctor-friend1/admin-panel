import { createAsyncThunk } from '@reduxjs/toolkit';
import doctors from "../data/doctors";
import specializations from "../data/specializations";

export const doctorSignUp = createAsyncThunk('doctors/registerDoctor', async (user) => {
    // TODO: add backend url
    // const response = await fetch();
    // return (await response.json()).data;
    return user;
})

export const fetchDoctors = createAsyncThunk('doctors/fetchDoctors', async () => {
    // TODO: add backend url
    // const response = await fetch('https://reqres.in/api/users?delay=1');
    // return (await response.json()).data;
    return doctors;
});

export const fetchSpecializations = createAsyncThunk('doctors/fetchSpecializations', async () => {
    return specializations;
})

