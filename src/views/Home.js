import {useEffect, useState} from "react";
import Header from "../components/Headers/Header";
import Features from "../components/landing-page/Features";
import Services from "../components/landing-page/Services";
import About from "../components/landing-page/About";
import Testimonials from "../components/landing-page/Testimonials";
import Team from "../components/landing-page/Team";
import Contact from "../components/landing-page/Contact";
import JsonData from "../data/data.json";

const Home = () => {
    const [landingPageData, setLandingPageData] = useState({});

    useEffect(() => {
        setLandingPageData(JsonData);
    }, []);

    return (
        <>
            <Header data={landingPageData.Header}/>
            <Features data={landingPageData.Features}/>
            <About data={landingPageData.About}/>
            <Services data={landingPageData.Services}/>
            <Testimonials data={landingPageData.Testimonials}/>
            <Team data={landingPageData.Team}/>
            <Contact data={landingPageData.Contact}/>
        </>
    )
}

export default Home;
