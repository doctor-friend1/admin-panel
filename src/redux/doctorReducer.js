import {createSlice} from "@reduxjs/toolkit";
import {fetchDoctors, fetchSpecializations} from "../api/doctors";

const doctorsSlice = createSlice({
    name: "doctors",
    initialState: {
        doctors: [],
        specializations: []
    },
    extraReducers: builder => {
        builder.addCase(fetchDoctors.fulfilled, (state, action) => {
            state.doctors = action.payload
        })
        builder.addCase(fetchSpecializations.fulfilled, (state, action) => {
            state.specializations = action.payload;
        })
    }
})

export default doctorsSlice.reducer
